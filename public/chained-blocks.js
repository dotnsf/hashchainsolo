var blocks = [];
var stock_blocks = [];

var r;
var g;
var b;

var color_index = -1;
var select_index = -1;
var deleted_lines = 0;

var init_blocks_num = 0;
var init_blocks_num_max = 10;
var draw_status = 0;
var latest_id = null;

function setup() {
  createCanvas(1000, 500);

  //. ブロック初期化
  $.ajax({
    type: 'GET',
    url: '/validate',
    success: function( result ){
      if( result.status ){
        init_blocks_num = result.docs.length;
        if( init_blocks_num ){
          if( init_blocks_num > init_blocks_num_max ){
            for( var i = init_blocks_num - init_blocks_num_max; i < init_blocks_num; i ++ ){
              stock_blocks.push( result.docs[i] );
            }
          }else{
            stock_blocks = result.docs;
          }
          latest_id = result.docs[result.docs.length-1]._id;
          processStocks();
        }
      }else{
        console.log( result.error );
      }
    },
    error: function( err ){
      console.log( err );
    }
  });
}

function mouseClicked(){
  if( color_index == -1 ){
  }else{
    //console.log( '[' + mouseX + ',' + mouseY + ']')
    var idx_x = Math.floor( mouseX / 100 );
    var idx_y = Math.floor( mouseY / 100 );
    idx_x = ( ( idx_y + deleted_lines ) % 2 ? 9 - idx_x : idx_x );
    idx_y = 4 - idx_y;
    var idx = 10 * idx_y + idx_x;
    if( color_index >= idx && blocks[idx] ){
      select_index = blocks[idx].index;
      //console.log( '(' + idx + ')' + blocks[idx].id + ': ' + blocks[idx].index );

      $.ajax({
        type: 'GET',
        url: '/doc/' + blocks[idx].id,
        success: function( result ){
          console.log( result );
          if( result.status ){
            $('#result').html( JSON.stringify( result.doc, null, 2 ) );
          }else{
            $('#result').html( result.message );
          }
        },
        error: function(){
          console.log( 'ajax error.' );
          $('#result').html( 'ajax error.' );
        }
      });
    }else{
      select_index = -1;
      $('#result').html( '' );
    }
  }

  return false;
}

function draw() {
  background(245);

  for (var i = 0; i < blocks.length; i++) {
    blocks[i].draw();
    blocks[i].update();
    blocks[i].checkEdges();
  }
}

function Block(x, y, id, m, r, g, b){
  this.index = color_index;
  this.x = x;
  this.y = y;
  this.id = id;
  this.m = m;

  // start position
  this.pos = createVector(this.x, 0/*this.y*/);

  this.r = r;
  this.g = g;
  this.b = b;

  this.draw = function(){
    // Draw the block
    noStroke();
    fill(this.r, this.g, this.b);
    rect(this.pos.x, this.pos.y, 100, 100 );

    stroke( 255, 255, 255 );
    strokeWeight( 5 );
    line(this.pos.x, this.pos.y+50, this.pos.x+99, this.pos.y+50);
    line(this.pos.x, this.pos.y+99, this.pos.x+99, this.pos.y+99);
    strokeWeight( 3 );
    line(this.pos.x+25, this.pos.y, this.pos.x+25, this.pos.y+50);
    line(this.pos.x+75, this.pos.y+50, this.pos.x+75, this.pos.y+99);

    if( select_index > -1 && this.index == select_index ){
      //. 選択したブロックを強調表示
      stroke( 255, 0, 0 );
      strokeWeight( 5 );
      noFill();
      rect(this.pos.x, this.pos.y, 100, 100 );
    }
  };

  this.update = function(){
    this.pos.y += 10;
    //console.log( 'y = ' + this.y + ', pos.y = ' + this.pos.y );
  };

  this.checkEdges = function() {
    if (this.pos.y + 100 > this.y) {
      this.pos.y = this.y - 100;
      if( this.index == color_index ){
        if( blocks.length > 30 && color_index >= 39 && color_index % 10 == 9 ){
          //. ブロックを縦に一段ずらす
          for( var i = blocks.length - 1; i >= 10; i -- ){
            blocks[i].y = blocks[i-10].y;
          }

          blocks.splice(0, 10);
          deleted_lines ++;
        }
      }
    }
  };
}

function newBlock(doc){
  //latest_id = doc._id;
  color_index ++;

  var block_x = color_index % 10;
  var block_y = Math.floor( color_index / 10 );

  block_x = ( block_y % 2 ? 9 - block_x : block_x );
  block_y = ( color_index < 40 ? 5 - block_y : 2 );

  r = ( color_index % 4 ) * 64;
  g = ( Math.floor( color_index / 4 ) % 4 ) * 64;
  b = ( Math.floor( color_index / 16 ) % 4 ) * 64;

  blocks.push(new Block(block_x * 100, block_y * 100, doc._id, JSON.stringify( doc, null, 2 ), r, g, b));

  if( color_index + 1 == init_blocks_num ){
    setInterval( checkNewBlock, 10000 );
  }
}

function processStocks(){
  if( stock_blocks && stock_blocks.length ){
    newBlock( stock_blocks[0] );
    stock_blocks.splice(0, 1);
    setTimeout( processStocks, 1000 );
  }else{
    draw_status = 1;
  }
}

function checkNewBlock(){
  $.ajax({
    type: 'GET',
    url: '/validate',
    success: function( result ){
      if( result.status ){
        var b = false;
        for( var i = 0; i < result.docs.length; i ++ ){
          var doc = result.docs[i];
          if( latest_id == null ){
            latest_id = result.docs[i]._id;
            stock_blocks.push( result.docs[i] );
            b = true;
          }else{
            if( b ){
              stock_blocks.push( result.docs[i] );
              latest_id = result.docs[i]._id;
            }else{
              if( latest_id == null || latest_id == doc._id ){
                b = true;
              }
            }          
          }
        }

        if( stock_blocks.length > 0 ){
          draw_status = 0;
          processStocks();
        }
      }else{
        console.log( result.validateMessage );
      }
    },
    error: function( err ){
      console.log( err );
    }
  });
}
