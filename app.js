// app.js

var express = require( 'express' );
var fs = require( 'fs' );
var bodyParser = require( 'body-parser' );
var crypto = require( 'crypto' );
var jwt = require( 'jsonwebtoken' );
var multer = require( 'multer' );
var app = express();

var settings = require( './settings' );

//. https://www.npmjs.com/package/@cloudant/cloudant
var Cloudantlib = require( '@cloudant/cloudant' );
var cloudant = null;
var db = null;
var cors = settings.enablesolojs;

if( !settings.db_host ){
  cloudant = Cloudantlib( { account: settings.db_username, password: settings.db_password } );
}else{
  var url = settings.db_protocol + '://';
  if( settings.db_username && settings.db_password ){
    url += ( settings.db_username + ':' + settings.db_password + '@' );
  }
  url += ( settings.db_host + ':' + settings.db_port );
  cloudant = Cloudantlib( url );
}

if( cloudant ){
  cloudant.db.get( settings.db_name, function( err, body ){
    if( err ){
      if( err.statusCode == 404 ){
        cloudant.db.create( settings.db_name, function( err, body ){
          if( err ){
            db = null;
          }else{
            db = cloudant.db.use( settings.db_name );
            createDesignDocument();
          }
        });
      }else{
        db = cloudant.db.use( settings.db_name );
      }
    }else{
      db = cloudant.db.use( settings.db_name );
    }
  });
}

app.use( multer( { dest: './tmp/' } ).single( 'file' ) );

app.use( express.static( __dirname + '/public' ) );
app.use( bodyParser.urlencoded( { extended: true, limit: '10mb' } ) );
//app.use( bodyParser.urlencoded() );
app.use( bodyParser.json() );

app.post( '/doc', function( req, res ){
  res.contentType( 'application/json; charset=utf-8' );
  console.log( 'POST /doc' );
  //console.log( req.body );
  if( db ){
    if( !validateDocument( req.body ) ){
      res.status( 400 );
      res.write( JSON.stringify( { status: false, message: 'document may include key which is not allowed to use.' }, 2, null ) );
      res.end();
    }else{
      db.view( 'library', 'bytimestamp', { descending: true, limit: 1 }, function( err, body ){
        var prev_doc = null
        var prev_on_change = false;
        if( !err ){
          if( body.rows && body.rows.length ){
            prev_doc = body.rows[0].value;
          }
        }

        if( prev_doc && prev_doc._id && prev_doc.hashchainsolo_system.hash ){
          if( prev_doc.hashchainsolo_system.on_change ){
            //. トランザクション中
            res.status( 400 );
            res.write( JSON.stringify( { status: false, message: 'hashchainsolo is busy!' }, 2, null ) );
            res.end();
          }else{
            prev_doc.hashchainsolo_system.on_change = true;
            db.insert( prev_doc, function( err, body ){ //. update
              if( err ){
                res.status( 400 );
                res.write( JSON.stringify( { status: false, message: err }, 2, null ) );
                res.end();
              }else{
                prev_doc._rev = body.rev;
                var doc = {};
                doc.body = req.body;
                doc.hashchainsolo_system = {};
                doc.hashchainsolo_system.timestamp = ( new Date() ).getTime();
                doc.hashchainsolo_system.remote_ip = req.ip;
                doc.hashchainsolo_system.prev_hash = prev_doc.hashchainsolo_system.hash;

                //. attachment
                if( req.file && req.file.path ){
                  var path = req.file.path;
                  var type = req.file.mimetype;
                  var filename = req.file.originalname;

                  var bin = fs.readFileSync( path );
                  var bin64 = new Buffer( bin ).toString( 'base64' );

                  doc.body['_attachments'] = {
                    file: {
                      content_type: type,
                      data: bin64
                    }
                  };
                }

                //. encryption
                if( req.headers['x-hashchainsolo-key'] ){
                  var key = req.headers['x-hashchainsolo-key'];
                  var docbody = JSON.parse( JSON.stringify( doc.body ) );
                  var encbody = jwt.sign( docbody, key, {} );
                  doc.body = encbody;
                }

                //. nonce and hash
                var nonce = 0;
                var nonce_hash = null;
                do{
                  nonce ++;
                  doc.hashchainsolo_system.nonce = nonce;

                  var hash = crypto.createHash( 'sha512' );
                  hash.update( JSON.stringify( doc ) );
                  nonce_hash = hash.digest( 'hex' );
                }while( settings.zerodigit > 0 && countTopZero( nonce_hash ) < settings.zerodigit )

                doc.hashchainsolo_system.hash = nonce_hash;
                console.log( 'nonce = ' + nonce );
                console.log( JSON.stringify( doc, 2, null ) );

                db.insert( doc, function( err, body ){ //. insert
                  if( req.file && req.file.path ){ fs.unlink( req.file.path, function( e ){} ); }
                  if( err ){
                    console.log( err ); //. err : Invalid attachment stub in XXXXXX
                    res.status( 400 );
                    res.write( JSON.stringify( { status: false, message: err }, 2, null ) );
                    res.end();
                  }else{
                    console.log( body );
                    var doc_id = body.id;
                    var doc_rev = body.rev;
                    delete prev_doc.hashchainsolo_system['on_change'];
                    db.insert( prev_doc, function( err, body ){ //. update
                      if( err ){
                        res.status( 400 );
                        res.write( JSON.stringify( { status: false, message: err }, 2, null ) );
                        res.end();
                      }else{
                        delete body['rev'];
                        res.write( JSON.stringify( { status: true, message: body }, 2, null ) );
                        res.end();
                      }
                    });
                  }
                });
              }
            });
          }
        }else{
          var doc = {};
          doc.body = req.body;
          doc.hashchainsolo_system = {};
          doc.hashchainsolo_system.timestamp = ( new Date() ).getTime();
          doc.hashchainsolo_system.remote_ip = req.ip;
          doc.hashchainsolo_system.prev_hash = null;

          //. attachment
          if( req.file && req.file.path ){
            var path = req.file.path;
            var type = req.file.mimetype;
            var filename = req.file.originalname;

            var bin = fs.readFileSync( path );
            var bin64 = new Buffer( bin ).toString( 'base64' );

            doc.body['_attachments'] = {
              file: {
                content_type: type,
                filename: filename,
                data: bin64
              }
            };
          }

          //. encryption
          if( req.headers['x-hashchainsolo-key'] ){
            var key = req.headers['x-hashchainsolo-key'];
            var docbody = JSON.parse( JSON.stringify( doc.body ) );
            var encbody = jwt.sign( docbody, key, {} );
            doc.body = encbody;
          }

          //. nonce and hash
          var nonce = 0;
          var nonce_hash = null;
          do{
            nonce ++;
            doc.hashchainsolo_system.nonce = nonce;

            var hash = crypto.createHash( 'sha512' );
            hash.update( JSON.stringify( doc ) );
            nonce_hash = hash.digest( 'hex' );
          }while( settings.zerodigit > 0 && countTopZero( nonce_hash ) < settings.zerodigit )

          doc.hashchainsolo_system.hash = nonce_hash;

          db.insert( doc, function( err, body ){ //. insert
            if( req.file && req.file.path ){ fs.unlink( req.file.path, function( e ){} ); }

            if( err ){
              res.status( 400 );
              res.write( JSON.stringify( { status: false, message: err }, 2, null ) );
              res.end();
            }else{
              delete body['rev'];
              res.write( JSON.stringify( { status: true, message: body }, 2, null ) );
              res.end();
            }
          });
        }
      });
    }
  }else{
    res.status( 400 );
    res.write( JSON.stringify( { status: false, message: 'hashchainsolo is failed to initialize.' }, 2, null ) );
    res.end();
  }
});

app.get( '/doc/:id', function( req, res ){
  res.contentType( 'application/json; charset=utf-8' );
  var id = req.params.id;
  console.log( 'GET /doc/' + id );
  if( db ){
    db.get( id, { include_docs: true }, function( err, body ){
      if( err ){
        res.status( 400 );
        res.write( JSON.stringify( { status: false, message: err }, 2, null ) );
        res.end();
      }else{
        res.write( JSON.stringify( { status: true, doc: body }, 2, null ) );
        res.end();
      }
    });
  }else{
    res.status( 400 );
    res.write( JSON.stringify( { status: false, message: 'hashchainsolo is failed to initialize.' }, 2, null ) );
    res.end();
  }
});

app.get( '/doc/:id/attachment', function( req, res ){
  res.contentType( 'application/json; charset=utf-8' );
  var id = req.params.id;
  console.log( 'GET /doc/' + id + '/attachment' );
  if( db ){
    db.get( id, { include_docs: true }, function( err, body ){
      if( err ){
        res.status( 400 );
        res.write( JSON.stringify( { status: false, message: err }, 2, null ) );
        res.end();
      }else{
        //. body._attachments.(attachname) : { content_type: '', data: '' }
        if( body.body && body.body._attachments ){
          for( key in body.body._attachments ){
            var attachment = body.body._attachments[key];
            var data64 = null;
            var filename = null;
            if( attachment.content_type ){
              res.contentType( attachment.content_type );
              data64 = attachment.data;
              filename = attachment.filename;  //. 未使用
            }

            //. 添付画像バイナリを取得する
            if( data64 ){
              var buf = Buffer.from( data64, 'base64' );
              res.end( buf, 'binary' );
            }else{
              res.contentType( 'application/json; charset=utf-8' );
              res.status( 400 );
              res.write( JSON.stringify( { status: false, message: 'No data found.' }, 2, null ) );
              res.end();
            }
/*
            db.attachment.get( id, key, function( err, buf ){
              if( err ){
                res.contentType( 'application/json; charset=utf-8' );
                res.status( 400 );
                res.write( JSON.stringify( { status: false, message: err }, 2, null ) );
                res.end();
              }else{
                res.end( buf, 'binary' );
              }
            });
*/
          }
        }else{
          res.status( 400 );
          res.write( JSON.stringify( { status: false, message: 'No attachment found.' }, 2, null ) );
          res.end();
        }
      }
    });
  }else{
    res.status( 400 );
    res.write( JSON.stringify( { status: false, message: 'hashchainsolo is failed to initialize.' }, 2, null ) );
    res.end();
  }
});

app.get( '/docs', function( req, res ){
  res.contentType( 'application/json; charset=utf-8' );
  console.log( 'GET /docs' );
  if( db ){
    db.list( { include_docs: true }, function( err, body ){
      if( err ){
        res.status( 400 );
        res.write( JSON.stringify( { status: false, message: err }, 2, null ) );
        res.end();
      }else{
        var docs = [];
        body.rows.forEach( function( doc ){
          var _doc = JSON.parse(JSON.stringify(doc.doc));
          if( _doc._id.indexOf( '_' ) !== 0 ){
            //delete _doc['hashchainsolo_system']['hash'];
            //delete _doc['hashchainsolo_system']['prev_hash'];
            delete _doc['hashchainsolo_system']['on_change'];
            //delete _doc['_id'];
            //delete _doc['_rev'];
            docs.push( _doc );
          }
        });

        var result = { status: true, docs: docs };
        res.write( JSON.stringify( result, 2, null ) );
        res.end();
      }
    });
  }else{
    res.status( 400 );
    res.write( JSON.stringify( { status: false, message: 'hashchainsolo is failed to initialize.' }, 2, null ) );
    res.end();
  }
});

app.post( '/fetch', function( req, res ){
  res.contentType( 'application/json; charset=utf-8' );
  var keys = req.body.keys;
  console.log( 'POST /fetch' );
  if( db ){
    db.fetch( { keys: keys }, function( err, body ){
      if( err ){
        res.status( 400 );
        res.write( JSON.stringify( { status: false, message: err }, 2, null ) );
        res.end();
      }else{
        var docs = [];
        body.rows.forEach( function( doc ){
          var _doc = JSON.parse(JSON.stringify(doc.doc));
          if( _doc._id.indexOf( '_' ) !== 0 ){
            //delete _doc['hashchainsolo_system']['hash'];
            //delete _doc['hashchainsolo_system']['prev_hash'];
            delete _doc['hashchainsolo_system']['on_change'];
            //delete _doc['_id'];
            delete _doc['_rev'];
            docs.push( _doc );
          }
        });

        var result = { status: true, docs: docs };
        res.write( JSON.stringify( result, 2, null ) );
        res.end();
      }
    });
  }else{
    res.status( 400 );
    res.write( JSON.stringify( { status: false, message: 'hashchainsolo is failed to initialize.' }, 2, null ) );
    res.end();
  }
});

app.post( '/decrypt', function( req, res ){
  res.contentType( 'application/json; charset=utf-8' );
  console.log( 'POST /decrypt' );
  //console.log( req.body );
  if( req.body && req.body.body && req.body.key ){
    var key = req.body.key;
    var body = req.body.body;

    jwt.verify( body, key, function( err, decrypted ){
      if( err ){
        res.status( 400 );
        res.write( JSON.stringify( { status: false, message: 'Invalid signature' }, 2, null ) );
        res.end();
      }else{
        if( decrypted['iat'] ){ delete decrypted['iat']; }
        res.write( JSON.stringify( { status: true, body: decrypted }, 2, null ) );
        res.end();
      }
    });
  }else{
    res.status( 400 );
    res.write( JSON.stringify( { status: false, message: 'no body/key found in request body.' }, 2, null ) );
    res.end();
  }
});

app.get( '/validate', function( req, res ){
  res.contentType( 'application/json; charset=utf-8' );
  console.log( 'GET /validate' );
  if( db ){
    var validateResult = true;
    var validateMessage = 'validated';
    var params = {};

    var limit = ( req.query.limit ? parseInt( req.query.limit ) : 0 );
    if( limit ){
      params.limit = limit;
    }
    var sort = ( req.query.sort ? req.query.sort : '' );
    if( sort ){
      //params.sort = [ { '_id': 'desc' } ];
    }
    db.view( 'library', 'bytimestamp', params, function( err, body ){
      if( err ){
        res.status( 400 );
        res.write( JSON.stringify( { status: false, message: err }, 2, null ) );
        res.end();
      }else{
        var docs = [];
        var prev_hash = null;
        body.rows.forEach( function( doc ){
          var _doc = JSON.parse(JSON.stringify(doc.value));
          var _hash = _doc['hashchainsolo_system']['hash'];
          var _prev_hash = _doc['hashchainsolo_system']['prev_hash'];
          delete _doc['hashchainsolo_system']['hash'];
          //delete _doc['hashchainsolo_system']['prev_hash'];
          delete _doc['hashchainsolo_system']['on_change'];
          delete _doc['_id'];
          delete _doc['_rev'];

          if( prev_hash != _prev_hash ){
            validateResult = false;
            validateMessage = 'invalid prev_hash value at chain #' + docs.length;
          }else{
            var hash = crypto.createHash( 'sha512' );
            hash.update( JSON.stringify( _doc ) );
            var doc_hash = hash.digest( 'hex' );
            if( doc_hash != _hash ){
              validateResult = false;
              validateMessage = 'invalid hash value at chain #' + docs.length;
            }else{
              delete doc.value._rev;
              docs.push( doc.value );
            }
          }

          prev_hash = _hash;
        });

        if( !validateResult && docs.length != body.rows.length ){
          //validateResult = false;
          //validateMessage = 'invalid result length ' + docs.length + ' out of ' + body.rows.length;
        }

        var result = { status: true, docs: docs };
        if( validateResult ){
          result.validateResult = validateResult;
          result.validateMessage = validateMessage;
        }else{
          result = { status: false, validateResult: false, error: validateMessage };
        }
        res.write( JSON.stringify( result, 2, null ) );
        res.end();
      }
    });
  }else{
    res.status( 400 );
    res.write( JSON.stringify( { status: false, message: 'hashchainsolo is failed to initialize.' }, 2, null ) );
    res.end();
  }
});


app.get( '/reorg', function( req, res ){
  res.contentType( 'application/json; charset=utf-8' );
  console.log( 'GET /reorg' );
  if( db ){
    db.view( 'library', 'bytimestamp', {}, function( err, body ){
      if( err ){
        res.status( 400 );
        res.write( JSON.stringify( { status: false, message: err }, 2, null ) );
        res.end();
      }else{
        //. 競合ノードを探す
        var conflict = false;
        var prev_docs = {};
        var conflict_prev_hashes = [];
        body.rows.forEach( function( doc ){
          var _doc = JSON.parse(JSON.stringify(doc.value));
          var _id = _doc['_id'];
          var _hash = _doc['hashchainsolo_system']['hash'];
          var _prev_hash = _doc['hashchainsolo_system']['prev_hash'];
          var _timestamp = _doc['hashchainsolo_system']['timestamp'];
          //var prev_doc = { id: _id, hash: _hash, timestamp: _timestamp };
          var prev_doc = _doc;
          if( _prev_hash == null ){ _prev_hash = '0'; }
          if( !prev_docs[_prev_hash] ){
            prev_docs[_prev_hash] = [ prev_doc ];
          }else{
            //. conclict!
            conflict_prev_hashes.push( _prev_hash );
            //prev_docs[_prev_hash].push( [ prev_doc ] );
            prev_docs[_prev_hash].push( prev_doc );
          }
        });

        if( conflict_prev_hashes && conflict_prev_hashes.length > 0 ){
          //. 競合ノード発見！
          conflict_prev_hashes.forEach( function( prev_hash ){
            var conflict_docs = prev_docs[prev_hash];

            //. 最大の timestamp を探す
            var max_timestamp = 0;
            conflict_docs.forEach( function( doc ){
              if( max_timestamp < doc.hashchainsolo_system.timestamp ){
                max_timestamp = doc.hashchainsolo_system.timestamp;
              }
            });

            if( max_timestamp > 0 ){
              console.log( 'max_timestamp=' + max_timestamp );
              //. チェーンから外すブロックを処理
              conflict_docs.forEach( function( doc ){
                console.log( doc );
                if( doc.hashchainsolo_system.timestamp < max_timestamp ){
                  //. このノードと、このノードの子孫をすべて処理

                  //. 「処理」をどうする？　削除？prev_hashのみ処理済みのものに書き換える？
                  //deleteTree( doc, prev_docs );
                  reorgTree( doc, prev_docs );
                }
              });
            }
          });

          var result = { status: true, result: "Conflicts processing.." };
          res.write( JSON.stringify( result, 2, null ) );
          res.end();
        }else{
          //. 競合ノードなし
          var result = { status: true, result: "No conflict found." };
          res.write( JSON.stringify( result, 2, null ) );
          res.end();
        }
      }
    });
  }else{
    res.status( 400 );
    res.write( JSON.stringify( { status: false, message: 'hashchainsolo is failed to initialize.' }, 2, null ) );
    res.end();
  }
});


/*
app.delete( '/doc/:id', function( req, res ){
  res.contentType( 'application/json; charset=utf-8' );
  var id = req.params.id;
  console.log( 'DELETE /doc/' + id );
  db.get( id, function( err, data ){
    if( err ){
      res.status( 400 );
      res.write( JSON.stringify( { status: false, message: err }, 2, null ) );
      res.end();
    }else{
      db.destroy( id, data._rev, function( err, body ){
        if( err ){
          res.status( 400 );
          res.write( JSON.stringify( { status: false, message: err }, 2, null ) );
          res.end();
        }else{
          res.write( JSON.stringify( { status: true }, 2, null ) );
          res.end();
        }
      });
    }
  });
});
*/


/*
 You need to create search index 'design/search' with name 'newSearch' in your Cloudant DB before executing this API.
 */
app.get( '/search', function( req, res ){
  res.contentType( 'application/json; charset=utf-8' );
  console.log( 'GET /search' );
  if( db ){
    var q = req.query.q;
    if( q ){
      db.search( 'library', 'newSearch', { q: q, include_docs: true }, function( err, body ){
        var ogp = {
          title: 'ねっぴ : 検索「',
          url: '',
          image_url: '',
          desc: ''
        };
        if( err ){
          res.status( 400 );
          res.write( JSON.stringify( { status: false, message: err }, 2, null ) );
          res.end();
        }else{
          if( body.bookmark ){ delete body.bookmark; }
          res.write( JSON.stringify( { status: true, result: body }, 2, null ) );
          res.end();
        }
      });
    }else{
      res.status( 400 );
      res.write( JSON.stringify( { status: false, message: 'parameter: q is required.' }, 2, null ) );
      res.end();
    }
  }else{
    res.status( 400 );
    res.write( JSON.stringify( { status: false, message: 'hashchainsolo is failed to initialize.' }, 2, null ) );
    res.end();
  }
});


app.post( '/reset', function( req, res ){
  res.contentType( 'application/json; charset=utf-8' );
  console.log( 'POST /reset' );
  if( db ){
    var passphrase = req.headers['x-passphrase'];
    if( settings.enablereset && passphrase == settings.db_name ){
      db.list( {}, function( err, body ){
        if( err ){
          res.status( 400 );
          res.write( JSON.stringify( { status: false, message: err }, 2, null ) );
          res.end();
        }else{
          var docs = [];
          body.rows.forEach( function( doc ){
            var _id = doc.id;
            if( _id.indexOf( '_' ) !== 0 ){
              var _rev = doc.value.rev;
              docs.push( { _id: _id, _rev: _rev, _deleted: true } );
            }
          });
          if( docs.length > 0 ){
            db.bulk( { docs: docs }, function( err ){
              res.write( JSON.stringify( { status: true, message: docs.length + ' documents are deleted.' }, 2, null ) );
              res.end();
            });
          }else{
            res.write( JSON.stringify( { status: true, message: 'No documents need to be deleted.' }, 2, null ) );
            res.end();
          }
        }
      });
    }else{
      res.status( 400 );
      res.write( JSON.stringify( { status: false, message: 'reset is not enabled. Change exports.resetenabled = true in settings.js' }, 2, null ) );
      res.end();
    }
  }else{
    res.status( 400 );
    res.write( JSON.stringify( { status: false, message: 'hashchainsolo is failed to initialize.' }, 2, null ) );
    res.end();
  }
});


app.get( '/js/solo.js', function( req, res ){
  //. have to enable bind_ip and CORS at first, and
  //. have to be called after '//cdn.jsdelivr.net/pouchdb/5.4.5/pouchdb.min.js' and '//cdnjs.cloudflare.com/ajax/libs/js-sha512/0.8.0/sha512.min.js'.
  res.contentType( 'application/javascript; charset=utf-8' );

  var js = "console.log( 'Cloudant/CouchDB CORS have to be enabled to use this feature.' );";
  if( cors ){
    var url = cors;
    if( typeof cors == 'boolean' ){
      url = settings.db_protocol + '://';
      if( settings.db_username && settings.db_password ){
        url += ( settings.db_username + ':' + settings.db_password + '@' );
        url += ( settings.db_username + '.cloudant.com' );
      }else{
        var host = req.headers.host;
        var tmp = host.split( ':' );
        if( tmp.length > 1 ){ host = tmp[0]; }
        url += ( host + ':' + settings.db_port );
      }
    }

    //. CORS restriction ?
    js = "//. have to be called after '//cdn.jsdelivr.net/pouchdb/5.4.5/pouchdb.min.js' and '//cdnjs.cloudflare.com/ajax/libs/js-sha512/0.8.0/sha512.min.js'.\n"
+  "var zerodigit = " + settings.zerodigit + ";\n"
+ "//. Local DB\n"
+ "var local_solo = new PouchDB( 'hashchainsolo' );\n"
+ "//. Remote DB\n"
+ "var remote_solo = new PouchDB( '" + url + "/hashchainsolo' );\n"
+ "\n"
+ "local_solo.destroy().then( function( res ){\n"
+ "  local_solo = new PouchDB( 'hashchainsolo' );\n"
+ "  //. sync\n"
+ "  //local_solo.replicate.from( remote_solo, {\n"
+ "  local_solo.sync( remote_solo, {\n"
+ "    live: true,\n"
+ "    retry: true\n"
+ "  }).on( 'paused', function( err ){\n"
+ "    //console.log( 'sync paused.' );\n"
+ "    //console.log( err );\n"
+ "    synced();\n"
+ "  }).on( 'error', function( err ){\n"
+ "    console.log( 'sync error.' );\n"
+ "    console.log( err );\n"
+ "  });\n"
+ "});\n"
+ "\n"
+ "async function hashchainsolo_get( id ){\n"
+ "  return new Promise( ( resolve, reject ) => {\n"
+ "    local_solo.get( id ).then( function( doc ){\n"
+ "      if( doc._rev ){ delete doc._rev; }\n"
+ "      resolve( doc );\n"
+ "    }).catch( function( err ){\n"
+ "      console.log( err );\n"
+ "      reject( err );\n"
+ "    });\n"
+ "  });\n"
+ "}\n"
+ "\n"
+ "async function hashchainsolo_post( body ){\n"
+ "  return new Promise( ( resolve, reject ) => {\n"
+ "    local_solo.query( 'library/bytimestamp', { descending: true, limit: 1 } ).then( function( result ){\n"
+ "      //console.log( result );\n"
+ "      var prev_doc = null;\n"
+ "      if( result.rows && result.rows.length ){\n"
+ "        prev_doc = result.rows[0].value;\n"
+ "        //console.log( prev_doc );\n"
+ "      }\n"
+ "      var doc = {}\n"
+ "      doc.body = body\n"
+ "      doc.hashchainsolo_system = {}\n"
+ "      doc.hashchainsolo_system.timestamp = ( new Date() ).getTime();\n"
+ "      doc.hashchainsolo_system.remote_ip = '';\n"
+ "      doc.hashchainsolo_system.prev_hash = prev_doc ? prev_doc.hashchainsolo_system.hash : null;\n"
+ "\n"
+ "      var nonce = 0;\n"
+ "      var nonce_hash = null;\n"
+ "      do{\n"
+ "        nonce ++;\n"
+ "        doc.hashchainsolo_system.nonce = nonce;\n"
+ "        var hash = sha512.create();\n"
+ "        hash.update( JSON.stringify( doc ) );\n"
+ "        nonce_hash = hash.hex();\n"
+ "      }while( zerodigit > 0 && countTopZero( nonce_hash ) < zerodigit )\n"
+ "      doc.hashchainsolo_system.hash = nonce_hash\n"
+ "      doc._id = uuid();\n"
+ "\n"
+ "      local_solo.put( doc ).then( function( response ){\n"
+ "        if( response && response.rev ){ delete response.rev; }\n"
+ "        resolve( response );\n"
+ "      }).catch( function( err ){\n"
+ "        reject( err );\n"
+ "      });\n"
+ "    }).catch( function( err ){\n"
+ "      console.log( err );\n"
+ "      if( err.status && err.status == 404 ){\n"
+ "        var doc = {}\n"
+ "        doc.body = body\n"
+ "        doc.hashchainsolo_system = {}\n"
+ "        doc.hashchainsolo_system.timestamp = ( new Date() ).getTime();\n"
+ "        doc.hashchainsolo_system.remote_ip = '';\n"
+ "        doc.hashchainsolo_system.prev_hash = null;\n"
+ "\n"
+ "        var nonce = 0;\n"
+ "        var nonce_hash = null;\n"
+ "        do{\n"
+ "          nonce ++;\n"
+ "          doc.hashchainsolo_system.nonce = nonce;\n"
+ "          var hash = sha512.create();\n"
+ "          hash.update( JSON.stringify( doc ) );\n"
+ "          nonce_hash = hash.hex();\n"
+ "        }while( zerodigit > 0 && countTopZero( nonce_hash ) < zerodigit )\n"
+ "        doc.hashchainsolo_system.hash = nonce_hash\n"
+ "        doc._id = uuid();\n"
+ "\n"
+ "        local_solo.put( doc ).then( function( response ){\n"
+ "          if( response && response.rev ){ delete response.rev; }\n"
+ "          resolve( response );\n"
+ "        }).catch( function( err ){\n"
+ "          reject( err );\n"
+ "        });\n"
+ "      }else{\n"
+ "        reject( null );\n"
+ "      }\n"
+ "    });\n"
+ "  });\n"
+ "}\n"
+ "\n"
+ "async function hashchainsolo_fetch( ids ){\n"
+ "  return new Promise( ( resolve, reject ) => {\n"
+ "    var doc_ids = [];\n"
+ "    ids.forEach( function( id ){\n"
+ "      doc_ids.push( { id: id } );\n"
+ "    });\n"
+ "    local_solo.bulkGet( { docs: doc_ids, include_docs: true } ).then( function( docs ){\n"
+ "      //console.log( docs );\n"
+ "      var results = [];\n"
+ "      docs.results.forEach( function( result ){\n"
+ "        if( result.docs[0].ok ){\n"
+ "          if( result.docs[0].ok._rev ){ delete result.docs[0].ok._rev; }\n"
+ "          results.push( result.docs[0].ok );\n"
+ "        }\n"
+ "      });\n"
+ "      resolve( results );\n"
+ "    }).catch( function( err ){\n"
+ "      console.log( err );\n"
+ "      reject( err );\n"
+ "    });\n"
+ "  });\n"
+ "}\n"
+ "\n"
+ "function hashchainsolo_decrypt( key, body ){\n"
+ "  //. No plan to implement this function for browser js.\n"
+ "  return null;\n"
+ "}\n"
+ "\n"
+ "function synced(){\n"
+ "}\n"
+ "\n"
+ "async function hashchainsolo_ledger(){\n"
+ "  return new Promise( ( resolve, reject ) => {\n"
+ "    local_solo.query( 'library/bytimestamp', {} ).then( function( result ){\n"
+ "      //console.log( result );\n"
+ "      var ledger = [];\n"
+ "      if( result.rows && result.rows.length ){\n"
+ "        for( var i = 0; i < result.rows.length; i ++ ){\n"
+ "          var doc = result.rows[i].value;\n"
+ "          if( doc._rev ){ delete doc._rev; }\n"
+ "          ledger.push( doc );\n"
+ "        }\n"
+ "      }\n"
+ "      //console.log( ledger );\n"
+ "      resolve( ledger );\n"
+ "    }).catch( function( err ){\n"
+ "      console.log( err );\n"
+ "      if( err.status && err.status == 404 ){\n"
+ "        resolve( [] );\n"
+ "      }else{\n"
+ "        reject( err );\n"
+ "      }\n"
+ "    });\n"
+ "  });\n"
+ "}\n"
+ "\n"
+ "function countTopZero( str ){\n"
+ "  var cnt = 0;\n"
+ "  while( str.length <= cnt || str.charAt( cnt ) == '0' ){\n"
+ "    cnt ++;\n"
+ "  }\n"
+ "  return cnt;\n"
+ "}\n"
+ "function uuid(){\n"
+ "  var uuid = '';\n"
+ "  for( var i = 0; i < 32; i ++ ){\n"
+ "    var r = Math.random() * 16 | 0;\n"
+ "    uuid += ( i == 12 ? 4 : ( i == 16 ? ( r & 3 | 8 ) : r ) ).toString( 16 );\n"
+ "  }\n"
+ "  return uuid;\n"
+ "}\n";
  }

  res.write( js );
  res.end();
});


app.get( '/dbinfo', function( req, res ){
  res.contentType( 'application/json; charset=utf-8' );
  console.log( 'GET /dbinfo' );
  if( db ){
    db.info( function( err, body ){
      if( err ){
        res.status( 400 );
        res.write( JSON.stringify( { status: false, message: err }, 2, null ) );
        res.end();
      }else{
        if( body.update_seq ){
          delete body.update_seq;
        }
        res.write( JSON.stringify( { status: true, info: body }, 2, null ) );
        res.end();
      }
    });
  }else{
    res.status( 400 );
    res.write( JSON.stringify( { status: false, message: 'hashchainsolo is failed to be initialized.' }, 2, null ) );
    res.end();
  }
});


function reorgTree( pdoc, prev_docs ){
  //console.log( pdoc );
  //console.log( prev_docs );
  var id = pdoc.id;
  var hash = pdoc.hashchainsolo_system.hash;
  reorgDocument( pdoc );
  var docs = prev_docs[hash];
  if( docs ){
    docs.forEach( function( doc ){
      reorgTree( doc, prev_docs );
    });
  }
}

function reorgDocument( doc ){
  console.log( "reorg document: " + doc._id );
  console.log( doc );
  var _id = doc['_id'];
  var _rev = doc['_rev'];
  delete doc['_id'];
  delete doc['_rev'];

  db.get( '_design/library/_view/bytimestamp', { descending: true, limit: 2 }, function( err, data ){
    if( err ){
      return false;
    }else{
      var prev_doc = null
      var prev_on_change = false;
      if( data && data.rows && data.rows.length ){
        prev_doc = data.rows[0].value;   //. { _id: '', _rev: '', name: '' }
        if( prev_doc._id == _id ){ prev_doc = data.rows[1].value; }
      }

      if( prev_doc && prev_doc._id && prev_doc.hashchainsolo_system.hash ){
      /*
        if( prev_doc.hashchainsolo_system.on_change ){
          //. トランザクション中
          return false;
        }else{
        */
          prev_doc.hashchainsolo_system.on_change = true;
          db.insert( prev_doc, function( err, body ){ //. update
            if( err ){
              return false;
            }else{

              prev_doc._rev = body.rev;

              doc.hashchainsolo_system = {};
              doc.hashchainsolo_system.timestamp = ( new Date() ).getTime();
              doc.hashchainsolo_system.prev_hash = prev_doc.hashchainsolo_system.hash;

              //. nonce and hash
              var nonce = 0;
              var nonce_hash = null;
              do{
                nonce ++;
                doc.hashchainsolo_system.nonce = nonce;

                var hash = crypto.createHash( 'sha512' );
                hash.update( JSON.stringify( doc ) );
                nonce_hash = hash.digest( 'hex' );
              }while( settings.zerodigit > 0 && countTopZero( nonce_hash ) < settings.zerodigit )

              doc.hashchainsolo_system.hash = nonce_hash;
              //doc.hashchainsolo_system.prev_hash = prev_doc.hashchainsolo_system.hash;

              doc._id = _id;
              doc._rev = _rev;
              db.insert( doc, function( err, body ){ //. insert
                if( err ){
                  return false;
                }else{
                  //console.log( body );
                  var doc_id = body.id;
                  var doc_rev = body.rev;
                  delete prev_doc.hashchainsolo_system['on_change'];
                  db.insert( prev_doc, function( err, body ){ //. update
                    if( err ){
                      return false;
                    }else{
                      return true;
                    }
                  });
                }
              });
            }
          });
          /*
        }
        */
      }
    }
  });
}


function deleteTree( ddoc, prev_docs ){
  var id = ddoc.id;
  var hash = ddoc.hash;
  deleteDocument( id );
  var docs = prev_docs[hash];
  docs.forEach( function( doc ){
    deleteTree( doc );
  });
}

function deleteDocument( doc_id ){
  console.log( "deleting document: " + doc_id );
  db.get( doc_id, function( err, data ){
    if( !err ){
      db.destroy( id, data._rev, function( err, body ){
      });
    }
  });
}

function sortDocuments( _docs ){
  var docs = [];
  for( var i = 0; i < _docs.length; i ++ ){
    var _doc = _docs[i];
    if( 'hashchainsolo_system' in _doc && 'timestamp' in _doc.hashchainsolo_system ){
      var b = false;
      for( var j = 0; j < docs.length && !b; j ++ ){
        if( docs[j].hashchainsolo_system.timestamp > _doc.hashchainsolo_system.timestamp ){
          docs.splice( j, 0, _doc );
          b = true;
        }
      }
      if( !b ){
        docs.push( _doc );
      }
    }
  }

  return docs;
}

function createDesignDocument(){
  var search_index_function = 'function (doc) { index( "default", doc._id ); }';
  if( settings.search_fields ){
    search_index_function = 'function (doc) { index( "default", ' + settings.search_fields + '.join( " " ) ); }';
  }

  //. デザインドキュメント作成
  var design_doc = {
    _id: "_design/library",
    language: "javascript",
    views: {
      bytimestamp: {
        map: "function (doc) { if( doc.hashchainsolo_system && doc.hashchainsolo_system.timestamp ){ emit(doc.hashchainsolo_system.timestamp, doc); } }"
      }
    },
    indexes: {
      newSearch: {
        "analyzer": settings.search_analyzer,
        "index": search_index_function
      }
    }
  };
  db.insert( design_doc, function( err, body ){
    if( err ){
      console.log( "db init: err" );
      console.log( err );
    }else{
      //console.log( "db init: " );
      //console.log( body );
    }
  } );
}

function countTopZero( str ){
  var cnt = 0;

  while( str.length <= cnt || str.charAt( cnt ) == '0' ){
    cnt ++;
  }

  return cnt;
}

function validateDocument( doc ){
  var r = true;

  if( typeof doc !== 'object' ){
    r = false;
  }else{
    /*
    if( 'id' in doc ){
      r = false;
    }
    if( 'rev' in doc ){
      r = false;
    }
    */
    if( 'hashchainsolo_system' in doc ){
      r = false;
    }
  }

  return r;
}


var port = process.env.PORT || 8080;
app.listen( port );
console.log( 'server started on ' + port );
