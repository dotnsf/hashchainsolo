//. create_conflict_by_force.js
//. Creating conflicted document by force. Use this only for test purpose.

var Cloudantlib = require( '@cloudant/cloudant' );
var crypto = require( 'crypto' );
var jwt = require( 'jsonwebtoken' );
var request = require( 'request' );

var settings = require( './settings' );

var user_id = null;

var cloudant = Cloudantlib( { account: settings.db_username, password: settings.db_password } );
var db = cloudant.db.use( settings.db_name );


function createRandomDoc(){
  var doc = {};
  doc.user_id = user_id;
  doc.moves = Math.floor( Math.random() * 60 ) + 70;
  doc.seconds = Math.floor( Math.random() * 40 ) + 40;
  doc.created = doc.updated = ( new Date() ).getTime();

  return doc;
}

function addDoc( prev_doc, doc ){
  return new Promise( function( resolve, reject ){
    if( prev_doc && prev_doc._id && prev_doc.hashchainsolo_system.hash ){
      if( prev_doc.hashchainsolo_system.on_change ){
        //. トランザクション中
        console.log( 'hashchainsolo is under transaction.' );
        reject( null );
      }else{
        prev_doc.hashchainsolo_system.on_change = true;
        db.insert( prev_doc, function( err, body ){ //. update
          if( err ){
            console.log( 'error: db.insert(1)' );
            console.log( err );

            reject( null );
          }else{
            prev_doc._rev = body.rev;
            var doc = {};
            doc.body = doc1;
            doc.hashchainsolo_system = {};
            doc.hashchainsolo_system.timestamp = ( new Date() ).getTime();
            doc.hashchainsolo_system.remote_ip = '127.0.0.1';
            doc.hashchainsolo_system.prev_hash = prev_doc.hashchainsolo_system.hash;

            //. encryption
            var key = user_id;
            var docbody = JSON.parse( JSON.stringify( doc.body ) );
            var encbody = jwt.sign( docbody, key, {} );
            doc.body = encbody;
  
            //. nonce and hash
            var nonce = 0;
            var nonce_hash = null;
            do{
              nonce ++;
              doc.hashchainsolo_system.nonce = nonce;
    
              var hash = crypto.createHash( 'sha512' );
              hash.update( JSON.stringify( doc ) );
              nonce_hash = hash.digest( 'hex' );
            }while( settings.zerodigit > 0 && countTopZero( nonce_hash ) < settings.zerodigit )

            doc.hashchainsolo_system.hash = nonce_hash;
  
            db.insert( doc, function( err, body ){ //. insert
              if( err ){
                console.log( 'error: db.insert(2)' );
                console.log( err );
  
                reject( null );
              }else{
                var doc_id = body.id;
                var doc_rev = body.rev;
                delete prev_doc.hashchainsolo_system['on_change'];
                
                db.insert( prev_doc, function( err, body ){ //. update
                  if( err ){
                    console.log( 'error: db.insert(3)' );
                    console.log( err );
  
                    reject( null );
                  }else{
                    var doc_id = body.id;
                    db.get( doc_id, { include_docs: true }, function( err, body ){
                      if( err ){
                        console.log( 'error: db.get(1)' );
                        console.log( err );
  
                        reject( null );
                      }else{
                        resolve( body );
                      }
                    });
                  }
                });
              }
            });
          }
        });
      }
    }else{
      var doc = {};
      doc.body = doc1;
      doc.hashchainsolo_system = {};
      doc.hashchainsolo_system.timestamp = ( new Date() ).getTime();
      doc.hashchainsolo_system.remote_ip = '127.0.0.1';
      doc.hashchainsolo_system.prev_hash = null;
  
      //. encryption
      var key = user_id;
      var docbody = JSON.parse( JSON.stringify( doc.body ) );
      var encbody = jwt.sign( docbody, key, {} );
      doc.body = encbody;
  
      //. nonce and hash
      var nonce = 0;
      var nonce_hash = null;
      do{
        nonce ++;
        doc.hashchainsolo_system.nonce = nonce;
  
        var hash = crypto.createHash( 'sha512' );
        hash.update( JSON.stringify( doc ) );
        nonce_hash = hash.digest( 'hex' );
      }while( settings.zerodigit > 0 && countTopZero( nonce_hash ) < settings.zerodigit )
  
      doc.hashchainsolo_system.hash = nonce_hash;
  
      db.insert( doc, function( err, body ){ //. insert
        if( err ){
          console.log( 'error: db.insert(4)' );
          console.log( err );
  
          reject( null );
        }else{
          var doc_id = body.id;
          db.get( doc_id, { include_docs: true }, function( err, body ){
            if( err ){
              console.log( 'error: db.get(2)' );
              console.log( err );
  
              reject( null );
            }else{
              resolve( body );
            }
          });
        }
      });
    }
  });
}

function countTopZero( str ){
  var cnt = 0;

  while( str.length <= cnt || str.charAt( cnt ) == '0' ){
    cnt ++;
  }

  return cnt;
}

if( process.argv.length < 3 ){
  console.log( "$ node create_conflict_by_force (user_id)" );
}else{
  user_id = process.argv[2];

  //. まず１つ追加して、その１つに分岐文書を作る
  var doc1 = createRandomDoc();
  console.log( 'doc1' );
  console.log( doc1 );

  //. POST /doc
  db.view( 'library', 'bytimestamp', { descending: true, limit: 1 }, function( err, body ){
    var prev_doc0 = null
    if( !err ){
      if( body.rows && body.rows.length ){
        prev_doc0 = body.rows[0].value;
      }
    }

    addDoc( prev_doc0, doc1 )
      .then( function( prev_doc1 ){
        if( prev_doc1 ){
          console.log( 'success: prev_doc1' );
          console.log( prev_doc1 );

          var doc1_1 = createRandomDoc();
          console.log( 'doc1_1' );
          console.log( doc1_1 );
  
          addDoc( prev_doc1, doc1_1 )
            .then( function( prev_doc1_1 ){
              if( prev_doc1_1 ){
                console.log( 'success: prev_doc1_1' );
                console.log( prev_doc1_1 );
  
                var doc1_2 = createRandomDoc();
                console.log( 'doc1_2' );
                console.log( doc1_2 );
  
                //. この時点で prev_doc1 の _rev が更新されているので、新たに取得する必要あり
                db.get( prev_doc1._id, {}, function( err, body ){
                  if( err ){
                    console.log( 'error: db.get(3)' );
                    console.log( err );
                  }else{
                    prev_doc1._rev = body._rev;
  
                    addDoc( prev_doc1, doc1_2 )
                      .then( function( prev_doc1_2 ){
                        console.log( 'success: prev_doc1_2' );
                        console.log( prev_doc1_2 );
                      })
                      .catch( function( err ){
                      });  
                  }
                });
              }
          })
         .catch( function( err ){
          });  
        }
      })
      .catch( function( err ){
      });  
  });
}



